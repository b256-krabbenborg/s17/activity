/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

function printUserInfo() {
	let fullName = prompt ("What is your name?");
	let age = prompt ("How old are you?");
	let location = prompt ("Where do you live?");
	alert("Thank you for your input!");
	console.log("Hello, " + fullName + ".");
	console.log("You are " + age + " years old.");
	console.log("You live in " + location + ".");
}

printUserInfo();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

function printTopFiveMovies () {
	console.log("1. Treasure Planet");
	console.log("Rotten Tomatoes Rating: 69%");
	console.log("2. Me Before You");
	console.log("Rotten Tomatoes Rating: 54%");
	console.log("3. The Butterfly Effect");
	console.log("Rotten Tomatoes Rating: 34%");
	console.log("4. The Lion King");
	console.log("Rotten Tomatoes Rating: 76%");
	console.log("5. In Time");
	console.log("Rotten Tomatoes Rating: 37%");
}

printTopFiveMovies ();

	//second function here:

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

function printUserFriends() {
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt ("Enter your first friend's name:");
	let friend2 = prompt ("Enter your second friend's name:");
	let friend3 = prompt ("Enter your third friend's name:");
	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
}

printUserFriends();
